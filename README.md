# Haptology Documentation

## Requirmends

## Windows

1. Download [file](https://github.com/rust-lang/mdBook/releases/download/v0.4.15/mdbook-v0.4.15-x86_64-pc-windows-msvc.zip)
2. Add mdbbook to system variable

## Usage
In main repo folder

```bash
   mdbook serve --open
```

```bash
   mdbook serve PATH/TO/BOOK --open
```
