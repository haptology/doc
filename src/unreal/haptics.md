# Haptics

Haptics are divided between Stream and Single.
- Single is a haptic that runs only for a certain amount of the time.
- Stream is a haptic that runs infinitely.

For a knowledge if haptic is Stream or Single refer to [Haptic Types](./../haptic_types/haptic_types.md).</br></br>


Invoking haptic requires to run below method
```cpp
Collision();
```

Incase it is Stream haptic there is also required to run
```cpp
StopCollision();
```
for stopping the haptic.

### Combining haptics

There is a possibility to combine Single and Stream haptic with each other. The outcome would be arithmetic average of two haptics.

## Plugin usage

1. Include Haptology plugin
```cpp
#include <Haptology.h>
```

2. Create HapticHandler object
```cpp
ControllersHandler controller;
```

3. Choose haptic type to vibrate.
```cpp
controller.setHapticType(Haptology::ControllersCollider::Type::Slider_hard);
```

4. Invoke the haptic.
```cpp
controller.Collision();
```

## Available Handlers

Handlers and their methods can be found inside plugin in `Haptology/Source/Haptology/Public` folder.

- FeedbackHandler
- EnvironmentHandler
- ControllersHandler
- TextureHandler

## Available Colliders

Colliders are used for segregating haptics by its meaning.</br>

Colliders are listed inside `Haptology/Source/Haptology/Public/HaptologyLibrary.h` file.</br>
Please refer to [Haptic Types](./../haptic_types/haptic_types.md) for full list with description of available haptics.</br>
Available colliders:

- TextureCollider
- ControllersCollider
- FeedbackCollider
- EnvironmentCollider