# Plugin structure

The most important files for developers are located in `Haptology/Source/Haptology/Public` folder.</br>
For instance:
- `HaptologyLibrary.h` file contains available haptics and gestures.
- there are present files with available handlers and their methods e.g. ControllersHandler


```
Haptology
├─ Binaries
│  ├─ HaptologyLibrary
│  │  └─ Win64
│  │     └─ hapticsApiWrapper.dll
│  └─ Win64
│     ├─ UE4Editor-Haptology.dll
│     └─ UE4Editor.modules
├─ Config
│  └─ FilterPlugin.ini
├─ Content
├─ Haptology.uplugin
├─ Intermediate
│  └─ Build
│     └─ Win64
│        ├─ UE4
│        │  ├─ Development
│        │  │  └─ Haptology
│        │  │     └─ Haptology.precompiled
│        │  └─ Shipping
│        │     └─ Haptology
│        │        └─ Haptology.precompiled
│        └─ UE4Editor
│           └─ Development
│              └─ Haptology
│                 └─ UE4Editor-Haptology.lib
├─ Resources
│  └─ Icon128.png
└─ Source
   └─ Haptology
      ├─ Haptology.Build.cs
      ├─ Private
      │  ├─ ControllersHandler.cpp
      │  ├─ EnvironmentHandler.cpp
      │  ├─ FeedbackHandler.cpp
      │  ├─ GestureReader.cpp
      │  ├─ Haptology.cpp
      │  └─ TextureHandler.cpp
      └─ Public
         ├─ ControllersHandler.h
         ├─ EnvironmentHandler.h
         ├─ FeedbackHandler.h
         ├─ GesturesReader.h
         ├─ Haptology.h
         ├─ HaptologyLibrary.h
         ├─ IHandler.h
         ├─ IHapticHandler.h
         └─ TextureHandler.h
```