# Gestures

Gestures are being read from haptic ring in 400ms interval.</br>
Invoking periodically `readGesture()` method is required for updating the structure of gesture containing data.</br>
Each gesture is a float value from 0 to 1, representing occurrence probability computed by the ring. </br>

## Plugin usage

1. Include Haptology plugin
```cpp
#include <Haptology.h>
```

2. Create `GestureReader` object
```cpp
GesturesReader gestureReader;
```

3. Invoke `readGesture()` method
```cpp
gestureReader.readGestures();
```

4. Check desired gesture attribute of `GestureReader` class.
```cpp
gestureReader.gestures.SwipeLeft
```

## Available gestures

```cpp
struct Gestures
{
    float DoubleTap;
    float Pinch;
    float Pull;
    float Push;
    float SingleTap;
    float SwipeDown;
    float SwipeLeft;
    float SwipeRight;
    float SwipeUp;
    float TurnLeft;
    float TurnRight;
};
```