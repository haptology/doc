# Unreal Engine Plugin

## About the plugin

Currently plugin is only available in C++ Unreal project.</br>
Plugin is only functional when [HapxLab](../installation/hapxlab.md) application is running on your computer.

## Setting up the plugin

1. Download `unreal_plugin.zip` file.
2. Unpack it in convenient for You location. Haptology folder should appear.
3. Copy Haptology folder to /Plugins folder in your Unreal Engine C++ project. if Plugins folder is not present, create it and copy folder accordingly.
4. Open or create C++ Unreal Engine project.
5. In Unreal Engine go to Edit->Plugins. In newly opened window search for Haptology. Check if plugin is enabled. If not enabled, enable it.
<div align="center">
    <img src="./img/haptology_plugin.png" width="100%"></img>
</div>

6. Navigate to {YourProjectPath}/{YourProjectName}/Source/{YourProjectName}/{YourProjectName}.Build.cs</br>
7. Open {YourProjectName}.Build.cs</br>
In newly opened file insert "Haptology" text to PublicDependencyModuleNames and PrivateDependencyModuleNames.
Save the file. File should look similar to below code.
```cpp
PublicDependencyModuleNames.AddRange(new string[] { "Core",
"CoreUObject", "Engine", "InputCore", "HeadMountedDisplay", "Haptology", });
PrivateDependencyModuleNames.AddRange(new string[] { "Haptology", });
```
8. Reopen your project in Unreal Engine.
9. Enjoy the plugin.</br>
##


For creating haptic sensation via plugin refer to [Haptics](./haptics.md).</br>
For reading gestures via plugin refer to [Gestures](./gestures.md).</br>
For plugin structure refer to [Plugin Structure](./plugin_structure.md).
