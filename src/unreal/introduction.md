# Unreal Engine

We provide plugin for [Unreal engine](https://www.unrealengine.com/en-US) developers. 

It allows you to feel touch sensations and use gestures to interact with objects in Unreal Engine.

### Before starting

Make sure that the [HapxLab](../installation/hapxlab.md) is running on your computer.

We recommend using Unreal in version `4.27.x`</br></br>
### Installation

For installation manual refer to [Unreal Plugin installation](./unreal/plugin.md)

