# HapxLab Installation
To connect the ring to your PC you need the `HapxLab application`
You may have to `grant extra permissions` to install and run the app 

 1. To `turn on` your Hapling press the center of the Haptology logo on your ring
 1. Hapling will light up with a white light
 1. Put the ring on your **right index finger, USB-C facing towards you** 

<br>
<div align="center">
    <img src="../hapling/img/ringdirection.svg" width="40%" ></img>
</div>
</br>


 1. Run the HapxLab application installer`
 2. Proceed with the instalation setup

<br>
<div align="center">
    <img src="install_png/install_3.png" width="70%" ></img>
</div>
</br>

 1. In `HapxLab`, you will go through the process of setting up your Hapling
 1. Click `Search for a new device`
 1. `Select` a Hapling and the `connect` button will appear, `click` it
 1. Hapling should vibrate, and you should see battery and range status if the connection was established
 1. By clicking `Next` you continue to the Setup phase  
 1. Click the `bell-shaped icon` to check if the ring vibrates, then click `Next`
 1. Try to `Swipe Right` in front of you, to check the gesture recognition feature
 1. If gesture recognition doesn't work, make sure you put the ring correctly
 1. By clicking `Save` you advance to naming your device
 1. Pick a name you like and `Save` it. You are all set up.

## Demo

 In the `HapxLab` app go to the `Haptic Library` where you can try out our Haptic presets.
 

######
