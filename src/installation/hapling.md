# Hapling

### Windows
 1. Plug in our Bluetooth dongle to your PC
  <div align="center">
    <img src="install_png/dongle.png.png" width="10%"></img>
</div>

 2. To connect via Bluetooth you need to `download Zadig`, a Windows application that installs generic USB drivers.
 3. Run it, open `Options` and select `List All Devices`

  <br>
<div align="center">
    <img src="install_png/install_1.png" width="70%"></img>
</div>

 4. Select `Generic Bluetooth Radio` and then `Reinstall Driver`
 <div align="center">
    <img src="install_png/install_2.png" width="70%"></img>
</div>

 5. To proceed with installation go to [HapxLab](./hapxlab.md)
