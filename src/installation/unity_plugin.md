# Unity Plugin



### Plugin Installation
1. Open UnityHub

![introduction_1](introduction_png/intro_1.png)

2. Create a new project in UnityHub 

![introduction_2](introduction_png/intro_2.png)

3. Download the `Haptology` file to your PC and add it to your Assets as a Custom Package to an element in the scene

![introduction_3](introduction_png/intro_3.png)

4. Import the `Haptology` file you have downloaded to the PC

![introduction_4](introduction_png/intro_4.png)

5. In the `Haptology` pack you will find a test scene in `Assets/Haptology/Example/Controller/TestScene.unity`. 
This is a simple example of haptic feedback.

![introduction_5](introduction_png/intro_5.png)
