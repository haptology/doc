# Plugin

Structure of `Haptology` Unity plugin:
```bash
│   Haptohandler.cpp
|   HaptoHandler.h
|   Library.h
|   main.cpp
│   
└───DLL
        hapticsApiWrapper.dll
```
### Haptohandler.cpp
`Source file` contains body of the methods available in header file.

### Haptohandler.h
Header file containing information about `API classes methods and variables`. 

### Library.h
This file contains information about `available gestures and haptic types`.

### main.cpp
This file contains `example code` with information how to implement and use all of the features.

### DLL
This folder contains all `Haptic Libraries` for the haptic handle, gesture handle and connection to the [HapxLab](../../hapxlab/introduction.md)