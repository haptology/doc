# C++
The c++ Plugin allows unlimited ways to integrate high-quality haptics without of the code implementation.

It allows you to feel touch sensations and use gestures to interact with objects in applications that can include c++ files.

### Before starting
Make sure that the [HapxLab](../installation/hapxlab.md) is running on your computer.
