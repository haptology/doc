# Test Scene

Example usage of Haptology C++ Plugin

### Code
Example of code:
```c++
#include "HaptoHandler.h"
#include <stdio.h>
#define SHORT_DELAY 200

void printGestures()
{
	GesturesReader gestureReader;
	if(gestureReader.ReadGestures())
	{
		printf("DoubleTap: %f\n",
			gestureReader.gestures.DoubleTap);
		printf("Pinch: %f\n",
			gestureReader.gestures.Pinch);
	}
}

int main(int argc, char const *argv[])
{
	ControllersHandler con;
	Haptology::ControllersCollider::Type con_type = Haptology::ControllersCollider::Type::Slider_smooth;

	if (con.getInstance())
	{
		if (con.setHapticType(con_type))
		{
            con.Collision();
            Sleep(SHORT_DELAY);
			con.StopCollision();
		}
	}

	printGestures();
	Sleep(SHORT_DELAY);
	printGestures();

	return 0;
}

```

This example code shows us how to handle `Stream` and `Gestures` in the system.

After `ControllersHandler` is initialized, type of haptic is declared.
After that `instance` for this object is initialized, haptic is assigned to it and after that haptic is turned on, works for 200 ms and then turned off. At the end gestures are printed two times with an interval of 200 ms.

All of this and more is available in `main.cpp` file.