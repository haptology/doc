# Haptic Behaviors

List of available `Haptic Behaviors`
- ControllerBehavior
- EnvironmentBehavior
- FeedbackBehavior
- TextureBehavior

We divide all haptic feedback into 2 categories:
 - `Signal`
 - `Stream`

### Signal
These haptics last about 1 second and you do not need to stop them.

### Stream
These haptics work on `ON-OFF` principle. You need to send `StopCollision` to stop.

To start you need to send `StartCollision`.

### Available haptics

For list consisting haptic Stream/Single division refer to [Haptic Types](./../haptic_types/haptic_types.md).