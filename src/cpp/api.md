# C++ Plugin API

This is a simple API Documentation for `Haptology c++ Plugin`
## Structure

We divide our API into 2 different modules

 - [HaptoHandler](./api/HaptoHandler.md)
 - [Gestures](./api/gestures.md)

