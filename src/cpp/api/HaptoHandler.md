# Hapto Handler

In the `HaptoHandler` you have access to four haptic classes and one class dedicated for reading gestures:
- ControllersHandler
- TextureHandler
- EnvironmentHandler
- FeedbackHandler
- GesturesReader

## Methods in haptic classes

#### `getInstance` - this method checkd if API was initialized and initialize it if not:

```c++
bool getInstance() override;
```

Returns `true` if success.

You can only init it once for one object.

Example:
```c++
ControllersHandler con;

if (!con.getInstance())
{
    std::cout << "failed to init!" << std::endl;
}
else
{
    std::cout << "API init success!"<< std::endl;
}
`````

#### `createInstance` - this method can be used to manually initialize API:
```c++
ControllersHandler con;

if (!con.createInstance())
{
    std::cout << "failed to create instance!" << std::endl;
}
else
{
    std::cout << "instance init success!"<< std::endl;
}
`````

#### `setHapticType` - this method sets the type of haptic you would like to use:
Example:
```c++
Haptology::ControllersCollider::Type con_type = Haptology::ControllersCollider::Type::Slider_smooth;

if (!con.setHapticType(con_type))
{
 
    std::cout << "Haptic was not set correctly" << std::endl;
}
else
{
    std::cout << "Haptic was set correctly" << std::endl;
}
```
#### `Collision` - this method inits a collision, so also vibrations on the device:
Example:
```c++
if (con.getInstance())
{
    if (con.setHapticType(con_type))
    {
            con.Collision();
    }
}
```
#### `StopCollision` - this method stops collision, so also stops vibrations on the device:
Example:
```c++
if (con.getInstance())
{
    if (con.setHapticType(con_type))
    {
            con.Collision();
            Sleep(300);
			con.StopCollision();
    }
}
```

#### BETA - `HandleIntensity` - this method allows to change haptic intensity:
This is a beta feature not implemented in firmware v0.0.2. Nevertheless, plugin in c++ is already prepared for this.
Example:
```c++
if (con.getInstance())
{
    if (con.setHapticType(con_type))
    {
            con.Collision();
			Sleep(SHORT_DELAY);
			con.HandleIntensity(1.0);
			Sleep(SHORT_DELAY);
			con.HandleIntensity(0.9);
			Sleep(SHORT_DELAY);
			con.HandleIntensity(0.8);
			Sleep(SHORT_DELAY);
			con.HandleIntensity(0.7);
			Sleep(SHORT_DELAY);
			con.HandleIntensity(0.6);
			Sleep(SHORT_DELAY);
			con.HandleIntensity(0.5);
			Sleep(SHORT_DELAY);
			con.HandleIntensity(0.4);
			Sleep(SHORT_DELAY);
			con.HandleIntensity(0.3);
			Sleep(SHORT_DELAY);
			con.HandleIntensity(0.2);
			Sleep(SHORT_DELAY);
			con.HandleIntensity(0.1);
			Sleep(SHORT_DELAY);
			con.StopCollision();
    }
}
```

## Methods in gesture class
#### `ReadGestures` - this method reads the probability table of gestures:
Example:
```c++
    GesturesReader gestureReader;
	if(gestureReader.ReadGestures())
	{
		printf("DoubleTap: %f\n",
			gestureReader.gestures.DoubleTap);
		printf("Pinch: %f\n",
			gestureReader.gestures.Pinch);
		printf("Pull: %f\n",
			gestureReader.gestures.Pull);
		printf("Push: %f\n",
			gestureReader.gestures.Push);
		printf("SingleTap: %f\n",
			gestureReader.gestures.SingleTap);
		printf("SwipeDown: %f\n",
			gestureReader.gestures.SwipeDown);
		printf("SwipeLeft: %f\n",
			gestureReader.gestures.SwipeLeft);
		printf("SwipeRight: %f\n",
			gestureReader.gestures.SwipeRight);
		printf("SwipeUp: %f\n",
			gestureReader.gestures.SwipeUp);
		printf("TurnLeft: %f\n",
			gestureReader.gestures.TurnLeft);
		printf("TurnRight: %f\n",
			gestureReader.gestures.TurnRight);
	}
```

