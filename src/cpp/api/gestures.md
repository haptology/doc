# Gestures
An object represents gestures in API
```C++
struct Gestures
	{
		float DoubleTap;
		float Pinch;
		float Pull;
		float Push;
		float SingleTap;
		float SwipeDown;
		float SwipeLeft;
		float SwipeRight;
		float SwipeUp;
		float TurnLeft;
		float TurnRight;
	};
```
The gesture value is from 0 to 1