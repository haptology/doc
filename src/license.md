# LICENSE

## IMPORTANT:
BEFORE USING THE SOFTWARE, PLEASE READ THIS END USER LICENSE
AGREEMENT (“EULA”) CAREFULLY. BY USING THE SOFTWARE YOU ARE
ACCEPTING THE TERMS OF THIS EULA. IF YOU DO NOT ACCEPT THE TERMS
OF THIS EULA, YOU MAY NOT USE THE SOFTWARE.

This EULA is a legal agreement between you and HAPTOLOGY SP. Z O.O.
(“HAPTOLOGY”). This EULA governs your rights and obligations regarding the
“HAPTOLOGY HAPxLAB” software, including application software and computer
software, of HAPTOLOGY and/or its third-party licensors (including HAPTOLOGY
Affiliates) and their respective affiliates (collectively, the “THIRD-PARTY
SUPPLIERS”), together with any updates/upgrades provided by HAPTOLOGY, any
printed, online or other electronic documentation for such software, and any data
files created by the operation of such software (collectively, the “SOFTWARE”).

Notwithstanding the foregoing, any software in the SOFTWARE having a separate
end user license agreement (including, but not limited to, GNU General Public
License and Lesser/Library General Public License) shall be covered by such
applicable separate end user license agreement in lieu of the terms of this EULA to
the extent required by such separate end user license agreement (“EXCLUDED
SOFTWARE”).

## SOFTWARE LICENSE

The SOFTWARE is licensed, not sold. The SOFTWARE is protected by copyright
and other intellectual property laws and international treaties.

## COPYRIGHT

All right and title in and to the SOFTWARE (including, but not limited to, any images,
photographs, animation, video, audio, music, text, and “applets” incorporated into the
SOFTWARE) is owned by HAPTOLOGY or one or more of the THIRD-PARTY
SUPPLIERS.

## GRANT OF LICENSE

HAPTOLOGY grants you a limited license to use the SOFTWARE solely in
connection with your compatible device (“DEVICE”) and only for your individual,
non-commercial use. HAPTOLOGY, HAPTOLOGY Affiliate (defined below), and the
THIRD-PARTY SUPPLIERS expressly reserve all rights, title, and interest (including,
but not limited to, all intellectual property rights) in and to the SOFTWARE that this
EULA does not specifically grant to you.

“HAPTOLOGY Affiliate” in this EULA shall mean HAPTOLOGY SP. Z O.O., which is
the ultimate parent company of HAPTOLOGY and any legal entities controlled by
HAPTOLOGY Sp. Z O.O.. The term “control” means the direct or indirect ownership
of at least fifty percent (50%) of the voting interest in such corporation or the power in
fact to control the management decisions of such an entity.

## REQUIREMENTS AND LIMITATIONS

You may not copy, publish, adapt, redistribute, attempt to derive source code, modify,
reverse engineer, decompile, or disassemble any of the SOFTWARE, whether in
whole or in part or create any derivative works from or of the SOFTWARE unless
such derivative works are intentionally facilitated by the SOFTWARE. You may not
modify or tamper with any digital rights management functionality of the SOFTWARE.
You may not bypass, modify, defeat or circumvent any of the functions or protections
of the SOFTWARE or any mechanisms operatively linked to the SOFTWARE. You
may not separate any individual component of the SOFTWARE for use on more than
one DEVICE unless expressly authorized to do so by HAPTOLOGY. You may not
remove, alter, cover or deface any trademarks or notices on the SOFTWARE. You
may not share, distribute, rent, lease, sublicense, assign, transfer or sell the
SOFTWARE. The software, network services, or other products other than
SOFTWARE upon which the SOFTWARE’S performance depends might be
interrupted or discontinued at the discretion of the suppliers (software suppliers,
service suppliers, or HAPTOLOGY or HAPTOLOGY Affiliate). HAPTOLOGY and
such suppliers do not warrant that the SOFTWARE, network services, or other
products will continue to be available, or will operate without interruption or
modification.

## EXCLUDED SOFTWARE AND OPEN SOURCE

## COMPONENTS

Notwithstanding the foregoing limited license grant, you acknowledge that the
SOFTWARE may include EXCLUDED SOFTWARE. Certain EXCLUDED
SOFTWARE may be covered by open source software licenses (“OPEN SOURCE
COMPONENTS”), which means any software licenses approved as open-source
licenses by the Open Source Initiative or any substantially similar licenses, including
but not limited to any license that, as a condition of distribution of the software
licensed under such license, requires that the distributor make the software available
in source code format. If and to the extent disclosure is required, please visit the [link](https://gitlab.com/haptology)
to our software that is open source or other HAPTOLOGY - designated website for a
list of applicable OPEN SOURCE COMPONENTS included in the SOFTWARE from
time to time, and the applicable terms and conditions governing its use. Such terms


and conditions may be changed by the applicable third party at any time without
liability to you. To the extent required by the licenses covering EXCLUDED
SOFTWARE, the terms of such licenses will apply in lieu of the terms of this EULA.
To the extent the terms of the licenses applicable to EXCLUDED SOFTWARE
prohibit any of the restrictions in this EULA with respect to such EXCLUDED
SOFTWARE, such restrictions will not apply to such EXCLUDED SOFTWARE. To
the extent the terms of the licenses applicable to OPEN SOURCE COMPONENTS
require HAPTOLOGY to make an offer to provide source code in connection with the
SOFTWARE, such an offer is hereby made.

## USE OF SOFTWARE WITH COPYRIGHTED MATERIALS

The SOFTWARE may be capable of being used by you to view, store, process and/or
use content created by you and/or third parties. Such content may be protected by
copyright, other intellectual property laws, and/or agreements. You agree to use the
SOFTWARE only in compliance with all such laws and agreements that apply to such
content. You acknowledge and agree that HAPTOLOGY may take appropriate
measures to protect the copyright of content stored, processed or used by the
SOFTWARE. Such measures include, but are not limited to, counting the frequency
of your backup and restoration through certain SOFTWARE features, refusal to
accept your request to enable restoration of data, and termination of this EULA in the
event of your illegitimate use of the SOFTWARE.

## CONTENT SERVICE

PLEASE ALSO NOTE THAT THE SOFTWARE MAY BE DESIGNED TO BE USED
WITH CONTENT AVAILABLE THROUGH ONE OR MORE CONTENT SERVICES
(“CONTENT SERVICE”). USE OF THE SERVICE AND THAT CONTENT IS
SUBJECT TO THE TERMS OF SERVICE OF THAT CONTENT SERVICE. IF YOU
DECLINE TO ACCEPT THOSE TERMS, YOUR USE OF THE SOFTWARE WILL BE
LIMITED. YOU ACKNOWLEDGE AND AGREE THAT CERTAIN CONTENT AND
SERVICES AVAILABLE THROUGH THE SOFTWARE MAY BE PROVIDED BY
THIRD PARTIES OVER WHICH SONY AND HAPTOLOGY AFFILIATE HAS NO
CONTROL. USE OF THE CONTENT SERVICE REQUIRES AN INTERNET
CONNECTION. THE CONTENT SERVICE MAY BE DISCONTINUED AT ANY TIME.
INTERNET CONNECTIVITY AND THIRD-PARTY SERVICES

You acknowledge and agree that access to certain SOFTWARE features may require
an Internet connection for which you are solely responsible. Further, you are solely
responsible for payment of any third-party fees associated with your Internet
connection, including but not limited to the Internet service provider or airtime
charges. Operation of the SOFTWARE may be limited or restricted depending on the
capabilities, bandwidth, or technical limitations of your Internet connection and
service. The provision, quality, and security of such Internet connectivity are the sole
responsibility of the third party providing such service.

## EXPORT AND OTHER REGULATIONS

You agree to comply with all applicable export and re-export restrictions and
regulations of the area or country in which you reside, and not to transfer, or
authorize the transfer, of the SOFTWARE to a prohibited country or otherwise in
violation of any such restrictions or regulations.

## HIGH-RISK ACTIVITIES

The SOFTWARE is not fault-tolerant and is not designed, manufactured, or intended
for use or resale as online control equipment in hazardous environments requiring
fail-safe performance, such as in the operation of nuclear facilities, aircraft navigation
or communication systems, air traffic control, direct life support machines, or
weapons systems, in which the failure of the SOFTWARE could lead to death,
personal injury, or severe physical or environmental damage (“HIGH-RISK
ACTIVITIES”). HAPTOLOGY, HAPTOLOGY Affiliate, each of the THIRD-PARTY
SUPPLIERS and each of their respective affiliates specifically disclaim any express
or implied warranty, duty, or condition of fitness for HIGH-RISK ACTIVITIES.

## EXCLUSION OF WARRANTY ON SOFTWARE

You acknowledge and agree that the use of the SOFTWARE is at your sole risk and
that you are responsible for use of the SOFTWARE. The SOFTWARE is provided
“AS IS”, without warranty, duty, or condition of any kind.

HAPTOLOGY, HAPTOLOGY AFFILIATE, AND EACH OF THE THIRD-PARTY
SUPPLIERS (for purposes of this Section, HAPTOLOGY, HAPTOLOGY Affiliate and
each of the THIRD-PARTY SUPPLIERS shall be collectively referred to as
“HAPTOLOGY”) EXPRESSLY DISCLAIM ALL WARRANTIES, DUTIES OR
CONDITIONS, EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY, NONINFRINGEMENT AND
FITNESS FOR A PARTICULAR PURPOSE. HAPTOLOGY DOES NOT WARRANT
OR MAKE ANY CONDITIONS OR REPRESENTATIONS (A) THAT THE
FUNCTIONS CONTAINED IN ANY OF THE SOFTWARE WILL MEET YOUR
REQUIREMENTS OR THAT THEY WILL BE UPDATED, (B) THAT THE
OPERATION OF ANY OF THE SOFTWARE WILL BE CORRECT OR ERROR-FREE
OR THAT ANY DEFECTS WILL BE CORRECTED, (C) THAT THE SOFTWARE
WILL NOT DAMAGE ANY OTHER SOFTWARE, HARDWARE OR DATA, (D) THAT
ANY SOFTWARE, NETWORK SERVICES (INCLUDING THE INTERNET) OR


PRODUCTS (OTHER THAN THE SOFTWARE) UPON WHICH THE SOFTWARE’S
PERFORMANCE DEPENDS WILL CONTINUE TO BE AVAILABLE,
UNINTERRUPTED OR UNMODIFIED, AND (E) REGARDING THE USE OR THE
RESULTS OF THE USE OF THE SOFTWARE IN TERMS OF ITS CORRECTNESS,
ACCURACY, RELIABILITY, OR OTHERWISE.
NO ORAL OR WRITTEN INFORMATION OR ADVICE GIVEN BY HAPTOLOGY OR
AN AUTHORIZED REPRESENTATIVE OF HAPTOLOGY SHALL CREATE A
WARRANTY, DUTY, OR CONDITION OR IN ANY WAY INCREASE THE SCOPE OF
THIS WARRANTY. SHOULD THE SOFTWARE PROVE DEFECTIVE YOU ASSUME
THE ENTIRE COST OF ALL NECESSARY SERVICING, REPAIR, OR
CORRECTION. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OF
IMPLIED WARRANTIES, SO THESE EXCLUSIONS MAY NOT APPLY TO YOU.
LIMITATION OF LIABILITY

HAPTOLOGY, HAPTOLOGY AFFILIATE, AND EACH OF THE THIRD-PARTY
SUPPLIERS (for purposes of this Section, HAPTOLOGY, HAPTOLOGY Affiliate, and
each of the THIRD-PARTY SUPPLIERS shall be collectively referred to as
“HAPTOLOGY”) SHALL NOT BE LIABLE FOR ANY INCIDENTAL OR
CONSEQUENTIAL DAMAGES FOR BREACH OF ANY EXPRESS OR IMPLIED
WARRANTY, BREACH OF CONTRACT, NEGLIGENCE, STRICT LIABILITY OR
UNDER ANY OTHER LEGAL THEORY RELATED TO THE SOFTWARE,
INCLUDING, BUT NOT LIMITED TO, ANY DAMAGES ARISING OUT OF LOSS OF
PROFITS, LOSS OF REVENUE, LOSS OF DATA, LOSS OF USE OF THE
SOFTWARE OR ANY ASSOCIATED HARDWARE, DOWNTIME AND USER’S
TIME, EVEN IF ANY OF THEM HAVE BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGES. IN ANY CASE, EACH AND ALL OF THEIR AGGREGATE
LIABILITY UNDER ANY PROVISION OF THIS EULA SHALL BE LIMITED TO THE
AMOUNT ACTUALLY PAID FOR THE SOFTWARE. SOME JURISDICTIONS DO
NOT ALLOW THE EXCLUSION OR LIMITATION OF INCIDENTAL OR
CONSEQUENTIAL DAMAGES, SO THE ABOVE EXCLUSION OR LIMITATION MAY
NOT APPLY TO YOU.

## AUTOMATIC UPDATE FEATURE

From time to time, HAPTOLOGY, HAPTOLOGY Affiliate, or the THIRD-PARTY
SUPPLIERS may automatically update or otherwise modify the SOFTWARE,
including, but not limited to, for purposes of enhancement of security functions, error
correction, and improvement of functions, at such time as you interact with
HAPTOLOGY’s, HAPTOLOGY Affiliate’s or third parties’ servers, or otherwise. Such
updates or modifications may delete or change the nature of features or other
aspects of the SOFTWARE, including, but not limited to, functions you may rely
upon. You acknowledge and agree that such activities may occur at HAPTOLOGY’s
sole discretion and that HAPTOLOGY may condition continued use of the
SOFTWARE upon your complete installation or acceptance of such updates or
modifications. Any updates/modifications shall be deemed to be and shall constitute
part of, the SOFTWARE for purposes of this EULA. By acceptance of this EULA, you
consent to such update/modification.

## ENTIRE AGREEMENT, WAIVER, SEVERABILITY

This EULA and HAPTOLOGY’s privacy policy, each as amended and modified from
time to time, together constitute the entire agreement between you and
HAPTOLOGY with respect to the SOFTWARE. The failure of HAPTOLOGY to
exercise or enforce any right or provision of this EULA shall not constitute a waiver of
such right or provision. If any part of this EULA is held invalid, illegal, or
unenforceable, that provision shall be enforced to the maximum extent permissible so
as to maintain the intent of this EULA, and the other parts will remain in full force and
effect. The United Nations Convention on Contracts for the International Sale of
Goods shall not apply to the SOFTWARE or this EULA. Furthermore, this EULA will
not be governed or interpreted in any way by referring to any law based on the U.S.
Uniform Computer Information Transactions Act (UCITA), any other act derived from
or related to UCITA, or any other similar act in applicable jurisdictions.

## BINDING ARBITRATION

UNLESS OTHERWISE RESTRICTED UNDER APPLICABLE LAWS, ANY
"DISPUTE" THAT IS NOT RESOLVED THROUGH THE INFORMAL NEGOTIATION
PROCESS DESCRIBED ABOVE SHALL BE RESOLVED EXCLUSIVELY

THROUGH BINDING ARBITRATION. "DISPUTE" is defined as any disagreement,
cause of action, claim, controversy, or proceeding between you and any
HAPTOLOGY entity related to or arising out of the SOFTWARE or this EULA.
DISPUTE is to be given the broadest possible meaning that will be enforced. If a
DISPUTE arises, you agree to first give notice to HAPTOLOGY by contacting
HAPTOLOGY SP. Z O.O. at Zebrzydowice 228, 34-130 Zebrzydowice (POLAND)
and engaging in good faith negotiations to attempt to resolve any DISPUTE for at
least 30 days, except that you or HAPTOLOGY (or any of HAPTOLOGY Affiliate)
may skip this informal negotiation procedure for DISPUTE enforcing, protecting, or
concerning the validity of intellectual property rights.

## ARBITRATION INSTRUCTIONS

To begin an arbitration, either you or HAPTOLOGY must make a written demand to
the other for arbitration. The arbitration will take place before a single arbitrator. It will
be administered in keeping with the Expedited Procedures of the Commercial


Arbitration Rules, and the Supplementary Proceedings for Consumer-Related
disputes when applicable (“Rules”) of the Polish Chamber of Commerce (“PCC”) in
effect when the claim is filed. You may get a copy of PCC’s Rules by contacting PCC.
The filing fees to begin and carry out arbitration will be shared between you and
HAPTOLOGY, but in no event shall your fees ever exceed the amount allowable by
the special rules for Consumers DISPUTE provided for by PCC, at which point
HAPTOLOGY will cover all additional administrative fees and expenses. This does
not prohibit the arbitrator from giving the winning party their fees and expenses of the
arbitration when appropriate pursuant to the Rules. Unless you and HAPTOLOGY
agree differently, the arbitration will take place in Warsaw, Poland, and applicable
laws in Poland shall govern the substance of any DISPUTE. The arbitrator’s decision
will be binding and final. The arbitrator may award declaratory or injunctive relief only
in favor of the party seeking relief, and only to the extent necessary to provide relief
warranted by that party’s individual claim. Any court with jurisdiction over the parties
may enforce the arbitrator’s decision.

## OPT-OUT INSTRUCTIONS
IF YOU DO NOT WISH TO BE BOUND BY THE BINDING ARBITRATION
PROVISION ABOVE, THEN: (1) YOU MUST NOTIFY HAPTOLOGY IN WRITING
WITHIN 30 DAYS OF THE DATE THAT YOU FIRST USE THE SOFTWARE OR
AGREE TO THIS EULA, WHICHEVER OCCURS FIRST; (2) YOUR WRITTEN
NOTIFICATION MUST BE MAILED TO HAPTOLOGY SP. Z O.O., ZEBRZYDOWICE
228, 34-130 ZEBRZYDOWICE, POLAND; AND (3) YOUR WRITTEN NOTIFICATION
MUST INCLUDE: (A) YOUR NAME; (B) YOUR ADDRESS; (C) THE DATE YOU
FIRST USED THE SOFTWARE OR AGREED TO THIS EULA; AND (D) A CLEAR
STATEMENT THAT YOU DO NOT WISH TO RESOLVE DISPUTES WITH ANY
HAPTOLOGY ENTITY THROUGH ARBITRATION.

Opting out of this dispute resolution procedure will not affect the terms and conditions
of this EULA, which will still apply to you.

## REJECTING CHANGES MADE TO THE DISPUTE

## PROCEDURES

Despite anything to the contrary in this EULA, you may reject changes made to the
binding arbitration provision if: (1) you’ve already begun authorized use of the
SOFTWARE at the time the change was/is made; and (2) you mail written notice to
the address in the immediately preceding paragraph within 30 days after the particular 
change was/is made. Should such a situation arise, you will still be bound
by the DISPUTE procedures you previously agreed to and existing before the change
you rejected was made.


## MISCELLANEOUS

Any DISPUTE determined not subject to arbitration and not initiated in small claims
the court will be litigated by either party in Kraków, POLAND. The parties agree that the
judgment, decree, or order rendered by a court of last resort or a court of lower
jurisdiction from which no appeal has been taken in Kraków, Poland shall be final and
binding upon both parties.

## EQUITABLE REMEDIES

Notwithstanding anything contained in this EULA to the contrary, you acknowledge
and agree that any violation of or non-compliance with this EULA by you will cause
irreparable harm to HAPTOLOGY, for which monetary damages would be
inadequate, and you consent to HAPTOLOGY obtaining any injunctive or equitable
relief that HAPTOLOGY deems necessary or appropriate in such circumstances.
HAPTOLOGY may also take any legal and technical remedies to prevent violation of
and/or to enforce this EULA, including, but not limited to, the immediate termination
of your use of the SOFTWARE, if HAPTOLOGY believes in its sole discretion that
you are violating or intend to violate this EULA. These remedies are in addition to any
other remedies HAPTOLOGY may have at law, in equity, or under contract.

## TERMINATION

Without prejudice to any of its other rights, HAPTOLOGY may terminate this EULA if
you fail to comply with any of its terms. In case of such termination, you must: (i)
cease all use, and destroy any copies, of the SOFTWARE; (ii) comply with the
requirements in the section below entitled “Your Account Responsibilities”.

## AMENDMENT
HAPTOLOGY RESERVES THE RIGHT TO AMEND ANY OF THE TERMS OF THIS
EULA AT ITS SOLE DISCRETION BY POSTING NOTICE ON A HAPTOLOGY
DESIGNATED WEBSITE, BY EMAIL NOTIFICATION TO AN EMAIL ADDRESS
PROVIDED BY YOU, BY PROVIDING NOTICE AS PART OF THE PROCESS IN
WHICH YOU OBTAIN UPGRADES/UPDATES OR BY ANY OTHER LEGALLY

RECOGNIZABLE FORM OF NOTICE. If you do not agree to the amendment, you
should promptly contact HAPTOLOGY for instructions. Your continued use of the
SOFTWARE after the effective date of any such notice shall be deemed your
agreement to be bound by such amendment.


## THIRD-PARTY BENEFICIARIES

Each THIRD-PARTY SUPPLIER is an express intended third-party beneficiary of and
shall have the right to enforce, each provision of this EULA with respect to the
SOFTWARE of such party.

## YOUR ACCOUNT RESPONSIBILITIES

Should you return your DEVICE to its place of purchase, sell or otherwise transfer
your DEVICE, or if this EULA is terminated, you are responsible for and must
uninstall the SOFTWARE from the DEVICE and delete any and all accounts you may
have established on DEVICE or are accessible through the SOFTWARE. You are
solely responsible for maintaining the confidentiality of any accounts you have with
HAPTOLOGY, HAPTOLOGY Affiliate or third parties and any usernames and
passwords associated with your use of the DEVICE.

Should you have any questions concerning this EULA, you may contact
HAPTOLOGY by writing to HAPTOLOGY SP. Z O.O. at Zebrzydowice 228, 34-130
Zebrzydowice (POLAND)

#### © 2022, HAPTOLOGY SP. Z O.O.


