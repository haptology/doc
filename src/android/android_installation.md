# HapxLab Android Installation

To install the HapxLab Android application, you need to download and install `SideQuest Advanced Installer` on your PC.

1. Download the `haptics-api.apk`
2. Connect your VR headset to the PC via USB-C cable
3. On your computer, in the SideQuest app, find the `install apk` on the top menu bar
4. Install `haptics-api.apk` via SideQuest
5. The installed application can be found in `App Library` on your VR headset