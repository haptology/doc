# Android App

Install HapxLab Android on your VR headset
### Connecting a new device


1. Open `HapxLab Android` app on your VR headset
2. Grab the small white bar below the app and move the HapxLab Android slightly upwards

<br>
<div align="center">
    <img src="./img/first-step.jpg" width="65%" ></img>
</div>
</br>

3. Grab the black bar under the app and move the HapxLab Android from the main view to the left or right view

<br>
<div align="center">
    <img src="./img/second-step.jpg" width="65%" ></img>
</div>
</br>

4. Click `SEARCH FOR NEW DEVICES` to search for available devices
5. Turn on your Hapling   
6. Select Hapling and switch `CONNECT` button 
7. Hapling should vibrate and the LED behaviour should change from blinking to constant light if the connection was established successfully
8. You should see the name and address of the connected Hapling in your HapxLab Android if the connection was established


### Disconnecting from the device


1. Click `DISCONNECT` button placed under the information of the connected device  
2. Hapling should vibrate and the white LEDs in the ring should start blinking
3. The app should allows you to search for new devices again


### Creating a server


The server is created automatically when you are connected to the Hapling  

### Important informations

<b>
Click `DISCONNECT` to close the server properly, when you no longer use Hapling.

Do not try to minimize the HapxLab Android application when you are connected to the Hapling.
</b>

