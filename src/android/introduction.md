# HapxLab Android




The `HapxLab Android` allows you to experience haptic sensations while playing games in a VR headset.
After installing the app on the device, you don't need a USB dongle, nor HapxLab desktop application, to connect to the Hapling. The app is stand-alone.

The application receives information about the start and end of the haptic and forwards that information to the Hapling.

A connection with the `HapxLab Android` app via Bluetooth is **necessary** to run `Hapling`.  



<br>
<div align="center">
    <img src="./img/android.jpg" width="45%"></img>
</div>


