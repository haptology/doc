# Haptic Types

Haptics are divided between Stream and Single.
- Single is a haptic that runs only for a certain amount of the time.
- Stream is a haptic that runs infinitely.

### Combining haptics

There is a possibility to combine Single and Stream haptic with each other. The outcome would be arithmetic average of two haptics.


## Haptic lists

Below list consists of available haptics divided to Single and Stream.</br>
Those haptics are present both in Unreal Engine and Unity.

### Controllers

| Haptic name | Single/Stream | Duration [s] |
| :---------: | :-----------: | :----------: |
| Reel | Single | 3 |
| Catalogue | Single | 0.301 |
| Endurance_test | Stream | ∞ |
| Slider_hard | Single | 1 |
| Slider_smooth | Single | 1 |
| Zipper | Single | 1.5 |
| Electric_razor | Stream | ∞ |
| Wand | Stream | ∞ |
| Spring_impulse | Single | 0.6 |

### Environment

| Haptic name | Single/Stream | Duration [s] |
| :---------: | :-----------: | :----------: |
| Spray | Stream | ∞ |
| Putting_helmet_on | Single | 0.8 |
| Error_waterlanding | Single | 1.3 |
| Boiling_water | Stream | ∞ |
| Finger_in_mud | Stream | ∞ |
| Fire | Stream | ∞ |
| Ghost | Stream | ∞ |
| Subtle_callback | Single | 0.4 |
| Pebbles | Stream | ∞ |
| Raining | Stream | ∞ |
| Wings_flapping | Single | 0.65 |
| Water | Stream | ∞ |
| Bone_fire | Stream | ∞ |
| Fish_tail_hitting | Stream | ∞ |
| Helicopter | Stream | ∞ |

### Feedback

| Haptic name | Single/Stream | Duration [s] |
| :---------: | :-----------: | :----------: |
| Button_sh | Single | 0.3 |
| Fitting_confirm | Single | 0.3 |
| Ramp_glide | Stream | ∞ |
| Connection02 | Single | 0.6 |
| Connection03 | Single | 0.5 |
| Connection04 | Single | 0.5 |
| Disconnection | Single | 0.8 |
| Contact | Single | 0.5 |
| Confirm | Single | 0.6 |
| Error | Single | 0.75 |
| Release | Single | 0.4 |
| Info | Single | 1 |
| Button | Single | 0.3 |
| Star | Single | 0.6 |
| Faulty_car_engine | Stream | ∞ |
| Helicopter_flying_over | Single | 5 |
| Starting_mower | Single | 0.4 |
| Sliding_on_ice_skates | Stream | ∞ |
| Chop_the_potato | Single | 0.12 |
| Sweep_the_floor | Single | 0.35 |
| Engine_fault_2 | Stream | ∞ |
| Sword_hit | Single | 0.14 |
| Car_on_asphalt | Stream | ∞ |
| Hapticformeasurement | Stream | ∞ |
| Bee_sting | Single | 0.05 |
| Fatal_failure_error | Stream | ∞ |
| Star_soft | Single | 0.7 |

### Texture

| Haptic name | Single/Stream | Duration [s] |
| :---------: | :-----------: | :----------: |
| Land_landing | Stream | ∞ |
| Jelly | Single | 2 |
| Gel | Single | 1.2 |
| Rought | Stream | ∞ |
| Sticky | Stream | ∞ |
| Smooth | Single | 1.5 |
| Liquid | Single | 0.3 |
| Granual | Stream | ∞ |
| Satin | Stream | ∞ |
| Snake_skin | Stream | ∞ |
| Lamb | Stream | ∞ |
| Sand | Stream | ∞ |
| Wood_bark | Stream | ∞ |
| Animal_fur | Stream | ∞ |
| Sand_granular | Stream | ∞ |
| Creamy_foam | Stream | ∞ |
| Octopus_arm | Stream | ∞ |