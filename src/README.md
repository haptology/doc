
<div align="center">
    <img src="branding/big_b.png" width="30%"></img>

<br>

At Haptology, we offer a comprehensive solution of software  
and hardware dedicated to Virtual World & Metaverse.  


We present a **Free-hand Controller and Haptic Library**,   
which enriches the audio-visual content  
with **haptic feedback**.  


The XR and gaming industries are the first step on the road  
to intuitive and immersive navigation of robotics and interfaces.  
</div> 
<br></br>

Shortcuts:
- I want to connect and setup my [Hapling](./installation/hapling.md)
- I want to install [HapxLab](./installation/hapxlab.md) application and feel the [Haptic Library](./hapxlab/desktop_app.md)
- I want to install [Unity Plugin](./installation/unity_plugin.md)
- I want to [test scene](./unity/example.md) in Unity

