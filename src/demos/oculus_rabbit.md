# Free the Rabbit

Main purpose of this short game is to show how haptic sensation works and how it can be applied by developers. Game was developed in Unity using SteamVR plugin, thus it runs on a PC and has to be streamed to the headset.

<div align="center">
    <img src="./img/free_the_rabbit_2.png" width="70%"></img>
</div>

## Requirements

Windows 10 PC with decent graphic card. Refer to [Specs](#specs) for details.</br>
You also need to install three applications on your PC to run the game:
- Steam
- SteamVR
- Oculus Quest 2 software installed on your PC.

### Steam Installation

For Steam application You need to create account in Steam Community.

#### Installation steps

1. Open the `Steam application`.
2. In the Steam application click the `Shop` button in the left corner.
3. Search `SteamVR`. <b> Note that </b> there is not only one search result, so beware of clicking another app.
4. Scroll down and click the green button `Play` or blue `Play now`.
5. Proceed with the installation. Default settings are alright.
6. Click `Finish` after successful installation.

### SteamVR Installation

For this tool You need to install Steam application first.</br>
SteamVR requires to have 6.3 GB of free space on your machine.

#### Installation steps

1. Open Steam application.
2. In Steam application click "Shop" button in left corner.
3. In search field type "SteamVR" and click enter.
4. In showed results click "SteamVR". Note that there are not only one search result, so beware of clicking another app.
5. Scroll down and click green button "Play" or blue "Play now".
6. Proceed with the installation. Default settings are alright.
7. Click Finish after successful installation.

### Oculus Quest 2 software installation.

Oculus Quest 2 application requires to have Oculus account or facebook account.
Make sure your PC meets [Specs](#specs) requirements.

#### Specs

| Requirement  | Requirement description |
| :---------: | :-----------------: |
| Processor | Intel ® i5-4590 / AMD Ryzen 5 1500X or better |
| Graphics | NVIDIA ® GeForce GTX 1070 or AMD 500 Series and higher |
| Memory | 8 GB RAM or more |
| USB ports | 1x USB 3.0 or newer (USB 3.2 Gen 1 or newer for best experience) |
| Ethernet port | Ethernet connection required for WiFi streaming only |
| Disc space | At least 15.3 GB of free space |
| Operating system | Windows ® 10 |


#### Installation steps

1. Go to [Meta Website](https://store.facebook.com/pl/en/quest/setup/).
2. Scroll down the page and click blue button "Download Software" under Quest 2 Air Link and Link cable tab.
3. OculusSetup.exe file should start downloading.
4. After downloading double click OculusSetup.exe file. Proceed with its installation.
5. Create or login to Oculus account. Creating account requires passing facebook account.
6. During configuration choose your Oculus Quest 2 googles and type of connecting to your googles. We recommend choosing wi-fi option. Type of connecting can also be adjusted or switched later on.

# Running game

Game can be run via wi-fi or by cable.

### Wi-fi variant


### Cable variant


# FAQ

