# Free the Bunny

Main purpose of this short game is to show how haptic sensation works and how it can be applied by developers. Game was developed in Unity using SteamVR plugin, thus it runs on a PC and has to be streamed to the headset.

<div align="center">
    <img src="./img/free_the_rabbit_2.png" width="70%"></img>
</div>

## Requirements

Windows 10 PC with decent graphic card. Refer to [Specs](#specs) for details.</br>
You also need to install three applications on your PC to run the game:
- Steam
- SteamVR
- HTC Vive Business Streaming

### Steam Installation

For Steam application You need to create account in Steam Community.

#### Installation steps

1. Open the `Steam application`.
2. In the Steam application click the `Shop` button in the left corner.
3. Search `SteamVR`. <b> Note that </b> there is not only one search result, so beware of clicking another app.
4. Scroll down and click the green button `Play` or blue `Play now`.
5. Proceed with the installation. Default settings are alright.
6. Click `Finish` after successful installation.

### SteamVR Installation

For this tool You need to install Steam application first.</br>
SteamVR requires to have 6.3 GB of free space on your machine.

#### Installation steps

1. Open Steam application.
2. In Steam application click `Shop` button in left corner.
3. In search field type `SteamVR` and click enter.
4. In showed results click `SteamVR`. Note that there are not only one search result, so beware of clicking another app.
5. Scroll down and click green button `Play` or blue `Play now`.
6. Proceed with the installation. Default settings are alright.
7. Click `Finish` after successful installation.

### HTC Vive Business Streaming Installation

For this tool make sure that your computer meets below specs.</br>

#### Specs

| Requirement  | Requirement description |
| :---------: | :-----------------: |
| Processor | Intel ® Core™ i5-4590 or AMD Ryzen 1500 equivalent or greater |
| Graphics | NVIDIA ® GeForce ® GTX 1060 or AMD Radeon RX 480 equivalent or greater |
| Memory | 6 GB RAM or more (Minimum may vary dependent on content) |
| USB ports | 1x USB 3.0 or newer (USB 3.2 Gen 1 or newer for best experience) |
| Ethernet port | Ethernet connection required for WiFi streaming only |
| Disc space | At least 1 GB of free space |
| Operating system | Windows ® 10 |

#### Installation steps
1. Go to [HTC Vive Streaming page](https://business.vive.com/eu/solutions/streaming/).
2. Scroll down the page and click `Download Software` blue button.
3. `VIVEBusinessStreamingInstaller.exe` should start downloading.
4. After downloading, double click downloaded app.
5. Proceed with the installation. Agree on application terms.
6. After installation, restart your computer.

# Running game

Game can be run via wi-fi or by cable.

### Wi-fi variant

1. Launch `SteamVR` and `HTC Business Streaming` on your PC.
2. Launch `Hapxlab` application and connect haptic ring to PC. For how to connect refer to [Desktop App](./hapxlab/desktop_app.md).
3. Connect HTC googles and PC to the same wi-fi. Wi-fi must have 5GHz speed.
4. In googles run `HTC Business Streaming` application. If application is not present, You might need to download it from the app store.
5. Connect app to your PC within your googles. After positive connection 
6. Run `Free the Bunny.exe` file on PC to start the game.

### Cable variant

1. Launch `SteamVR` and `HTC Business Streaming` on your PC.
2. Launch `Hapxlab` application and connect haptic ring to PC. For how to connect refer to [Desktop App](./hapxlab/desktop_app.md).
3. Connect HTC googles and PC with [USB-C]------[USB-3.0] cable.
4. Pop-up in HTC should appear. Choose the second option with streaming. If it remains greyed out, please replug the cable.
5. Run `Free the Bunny.exe` file on PC to start the game.

# FAQ

1. Vive Business Streaming app shows `Error 330 No graphical processor`.
    - You have not met application requirements. Refer to [Specs](#specs) for details.
2. Vive Business Streaming app shows `No driver available`.
    - It is common error with `HTC Vive Business Streaming` app. Plugging HTC Vive Focus 3 to the computer, or restarting computer my solve the issue.
3. Hands are not visible in the game.
    - Check if hands can be visible in HTC Vive Focus 3. Sometimes happens that googles are stopping showing hands. If not present in HTC main entering room, please restart googles. It is solving the issue.
4. I tried connecting googles with PC and I had run necessary applications before doing it, but the second option with streaming is greyed out and not clickable.
    - Try replugging cable with PC. If problem still remains, try to rerun `SteamVR` and `HTC Business Streaming` app on your PC.