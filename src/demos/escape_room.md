# Escape Room

Discover the potion recipe that will help you break free from this crazy laboratory.

In this demo, you can enjoy the sensations of casting a spell, touching a ghost, or petting a friendly flame. When discovering the recipe, certain objects may feel different than others.

<div align="center">
    <img src="./img/escape_room1.png" width="70%"></img>
</div>


## Installation 

To install the HapxLab Android application, you need to download and install SideQuest Advanced Installer on your PC.

1. Download the EscapeRoom.apk
2. Connect your VR headset to the PC via USB-C cable
3. On your computer, in the SideQuest app, find the install apk on the top menu bar
4. Install EscapeRoom.apk via SideQuest
5. The installed application can be found in App Library on your VR headset

## Gestures

### Grabbing and moving items

In the game it is possible to lift and move objects. 
1. Bring you hand close to the chosen item
2. The item should begin to highlight blue
3. Pick up the item and move it to the desired location

### Using a wand

In the game you can use a magic wand.
1. Pick up the wand and point it where you want to use it
2. While holding the wand, gently lift your thumb and then bring it back

### Teleportation

In the game you can teleport to places marked with a green circle. </br>
1. Point the palm of your hand upwards
2. Use your index finger to point to the place you want to teleport to
3. If teleportation is possible, the line showing the teleport direction will turn blue
4. Bend your finger to teleport to the selected place

The method of teleportation is shown below. </br>
To learn more about the teleportation mechanism refer to [MRTK Documentation](https://docs.microsoft.com/en-us/windows/mixed-reality/mrtk-unity/mrtk2/?view=mrtkunity-2022-05).

<div align="center">
    <img src="./img/handteleport.gif" width="70%"></img>
</div>



