# Hapling
<div align="center">
    <img src="img/haplingrc.png" width="15%"></img>
</div>

<br></br>

The Hapling is an advanced, **wireless controller** that allows you to feel a new dimension of *haptic sensations* by carefully setting the amplitude and frequency of vibrations.   
The intuitive *gesture recognition* feature allows seamless experience throughout multiple platforms. 

It combines electronics, mechanics, and powerful software, in the form of a lightweight and smart ring, tailored to your hand by an advanced *size adjustment* mechanism.

## Correct placement and direction 

The ring on your hand should look like this  

<br>
<div align="center">
    <img src="img/ringdirection.svg" width="30%" ></img>
</div>
</br>

## Sizing

Hapling comes in 5 sizes - XS, S, M, L and XL.  
These can be further tightened with the inner ring.

### How to check your Hap-size?

- Measure the circumference of your **right index finger**, e.g. with a strip of paper.  
Be careful not to use flexible materials - the measurement will be far from correct.  

- Take the measurement from the place **below the bone** in the middle of your finger.  
<br>
<div align="center">
    <img src="img/measurement.svg" width="30%"></img>
</div>
</br>

- Measure the designated fragment with a ruler. 

- Compare the circumference you get with the sizes below:  

<br>
<div align="center">
    <img src="img/sizes.png" width="40%" 
    alt="in millimeters: extra small 47 - 53, small 54 - 59, medium 60 - 66, large 67 - 72, extra large 73 - 78"></img>
</div>
</br>

It is necessary for the Hapling to remain in firm **contact with your skin** to provide the best experience. If you find yourself in between two sizes, pick a smaller one.  

## Connection
*For the installation and setup manual [click here](./../installation/hapling.md)*

Hapling requires you to connect via a `Bluetooth dongle` inserted into your computer.  
[`HapxLab`](/hapxlab/desktop_app.md) desktop application is required to connect the ring to the computer.  
Gac
You need to connect Hapling through the `HapxLab` app for the `Unity Plugin` to acquire the information from the ring.

## Charging
To charge `Hapling`, plug in the `USB-C` cable to a power adapter.

DO NOT CHARGE THE RING WITH YOUR COMPUTER'S `USB-C` PORT.

## Behavior

To `turn on` the Hapling, press the center of the logo button.  
To `turn off`, hold the button for ~4 seconds, until the light will go out

When Hapling establish a `connection` with the PC, it vibrates.  
The same vibration is used when it `disconnects`.

Hapling also communicates with the user with `light signals` in the upper part of the housing, under the logo button.
It has LEDs in three colors - white, blue and red. 

**At start:**  
White LEDs are blinking  

**Connection:**  
Not connected to the computer - white LEDs are blinking  
Connected to the computer - white LEDs are constantly on  

**Error status:**  
Low battery - red LED flashes every 2 seconds  
Partial hardware error - red LED flashes every 0.2 s  
Permanent hardware error - red LED is constantly on  

**Charging:**  
Blue LED are constantly on

## Specification

- Compatibility: Bluetooth 4.0 - UB 400
- System requirements: Ubuntu 18 +, Windows 10
- Work time: up to 4h
- Charge time: ~1,5h