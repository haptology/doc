# Test Scene

Example usage of Haptology Unity Plugin

### Scene

1. Before you add any other object, you have to add `HapticApiHandler.prefab` to the scene

![example_1](example_png/example_1.png)

2. Then you can add `HapticBehavior` to an element in the scene

![example_2](example_png/example_2.png)

3. Choose `Haptic Type` from the drop-down list

![example_3](example_png/example_3.png)

Example of code:
```c#
using UnityEngine;
using UnityEngine.UI;
using Haptology;
using System.Collections;
public class ButtonHandler : MonoBehaviour
{
    public Button mButton;
    public ControllersBehavior behavior;
    private bool mRunning = false;
    void Start()
    {
        mButton.onClick.AddListener(OnClick);
    }
    void OnClick()
    {
        if (!mRunning)
        {
            StartCoroutine(runBehavior());
        }

    }

    IEnumerator runBehavior()
    {
        mRunning = true;
        behavior.HapticCollision();
        yield return new WaitForSeconds(2);
        behavior.StopHapticCollision();
        mRunning = false;
    }
}
```

This example code shows us how to handle `Stream` in the system.

When you click on a button you send `HapticCollision`
After 2 seconds you send `StopHapticCollision` to stop the stream.

All of this is available in `ControllersScene` in `Example\Controller`