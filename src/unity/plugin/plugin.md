# Plugin

Structure of `Haptology` Unity plugin:
```bash
│   HapticApiHandler.prefab
│   
├───Example
│   └───Controllers
│           ButtonHandler.cs
│           TestScene.unity
│           
├───Scripts
│       ControllersBehavior.cs
│       EnviromentBehavior.cs
│       FeedbackBehavior.cs
│       GestureBehavior.cs
│       HapticApiBehavior.cs
│       IHapticBehavior.cs
│       TextureBehavior.cs
│       
└───x86_64
        haptics-api.dll
        hapticWrapper.dll
```
### HapticApiHandler.perfab

This prefab manages the connection to the [HapxLab](../../hapxlab/introduction.md) via websockets.

### Example
This folder contains `TestScene.unity` scene of Unity Plugin.

### Scripts:

This folder contains all [hapticBehavior](../hapticBehavior.md) available in API

### x86_64

This folder contains all `Haptic Libraries` for the haptic handle, gesture handle and connection to the [HapxLab](../../hapxlab/introduction.md)