# Haptic Api Wrapper

In the `HapticApiWrapper` you have access to methods:

#### `Init` - this method initializes API:
```c#
public static bool InitApi();
```


Return `true` if success.

You can only init it once.

Example:
```c#
if (!HapticApiWrapper.InitApi())
{
    Debug.Log("failed to init!");
}
else
{
    Debug.Log("API init success!");
}
`````
#### `Close` - this method closes API:
```c#
public static bool CloseApi();
```
Example:
```c#
void OnDestroy()
{
    HapticApiWrapper.CloseApi();
}
```

#### `Dispatch` - for working communication with HapxLab application:

```c#
public static void Dispatch();
```
This call have to be added in `Update loop`.

Example:
```c#
void Update()
{
    HapticApiWrapper.Dispatch();
}
```
#### `Read Gestures` - gives array of probability for [gestures](./gestures.md) :

```C#
public static Gestures ReadGestures();
```

Example:
```C#
var gestures = HapticApiWrapper.ReadGestures();
Debug.Log(gestures);
```
#### `Register Collider` - adds ICollider from API:
```C#
public static void RegisterCollider(IHapticCollision item);
```


#### `Unregister Collider` - removes Collider from API:
```C#
public static void UnregisterCollider(IHapticCollision item);
```
