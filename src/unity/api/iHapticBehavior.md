# IHapticBehavior

In `IHapticBehavior` you have access to methods:


This method start haptic feedback:
```C#
void HapticCollision();
```



This method stop haptic feedback:
```C#
void StopHapticCollision();
```


