# Unity
The Unity Plugin allows unlimited ways to integrate high-quality
haptics without requiring code or previous design experience.

It allows you to feel touch sensations and use gestures to interact with objects in Unity scenes.
### Before starting

Make sure that the [HapxLab](../installation/hapxlab.md) is running on your computer.

We recommend using Unity in version `2020.3`