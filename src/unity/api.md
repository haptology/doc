# Unity API

This is a simple API Documentation for `Haptology Unity Plugin`

## Structure

We divide our API into 3 different modules

 - [HapticApiWrapper](./api/hapticApiWrapper.md)
 - [IHapticBehavior](./api/iHapticBehavior.md)
 - [Gestures](./api/gestures.md)

