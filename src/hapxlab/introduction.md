# HapxLab



The `HapxLab` application contains a collection of **carefully crafted haptic signals** that translate into sensations on your hand during any assigned 
interaction.  

A connection with the `HapxLab` app via Bluetooth is **necessary** to run `Hapling`.  

<br>
<div align="center">
    <img src="../installation/install_png/scr1.jpg" width="80%"></img>
</div>


