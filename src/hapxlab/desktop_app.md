# Desktop App

*To install the app on your computer follow the instructions in [Installation](./../installation/hapxlab.md) chapter.*
<br>
<br>
<br>
In the menu, you can find the following categories:

## Haptic Library

- 24 haptics in 4 categories:  

*Controllers*, imitations of buttons, sliders, knobs, or even a magic wand.  
*Feedback*, haptic sensations that make interface notifications more attractive.  
*Textures*, tactile illusions during interaction, based on the external, visual properties of the object.  
*Environment*, sensations such as a splash of water, the buzzing of a bee, the crackling fire.

- 11 gestures  
Swipe ←,↑,→,↓,  
Turn ←,→,  
Push, pull,   
Tap, Double tap, Pinch.

## Control Panel

In Control Panel you can `connect a new device` or manage your currently `saved devices`.

### Connecting a new device


1. In `HapxLab`, go to `Control Panel` and click `Scan for a new device`  
2. Click `Search for a new device`  
3. `Select` a Hapling and the `connect` button will appear, `click` it  
4. Hapling should vibrate, and you should see battery and range status if the connection was established  
5. By clicking `Next` you continue to the Setup phase  
6. Click the `bell-shaped icon` to check if the ring vibrates, then click `Next`  
7. Try to `Swipe Right` in front of you, to check the gesture recognition feature  
8. By clicking `Save` you advance to naming your device  
9. Pick a name you like and `Save` it. You are all set up.  
 
### Rename Device
1. In `HapxLab`, in the `Control Panel` click `Devices`  
2. Make sure that the device that you want to rename is currently `connected`

<div align="center">
    <img src="features_png/rename_001.PNG" width="80%"></img>
</div>

3. Click on `rename`
4. Pick a new name you like and `Save` it. You are all set up.  

### Forget Device
1. To forget the device make sure that it is **disconnected** from `HapxLab`
2. In `HapxLab`, in the `Control Panel` click `Devices`  
3. Click on `Forget` below the chosen device 
4. Confirm that you want to forget this device 

### Saved Devices
You can `connect` to the previously added device. **Make sure it's turned on**.

1. In `HapxLab`, in the `Control Panel` click `Devices`  
2. Click on `Check ring availability` at the top of the window
3. If the ring is turned on and connection is possible, a new button `connect` should appear below it

<div align="center">
    <img src="features_png/reconnect_001.PNG" width="80%"></img>
</div>

4. Click on `Connect` below the chosen device
